function sorting(event) {
    var val = this.getAttribute('data-sort');
    var symbol = (document.URL.indexOf('?') === -1) ? '?' : '&' ;
    var url = (document.URL.indexOf('sort=') === -1) ? document.URL + symbol + 'sort=' + val : document.URL.replace(/sort=[\w]+/,'sort=' + val);
    document.location.href = url;
}


window.onload = function () {
    buttons = document.querySelectorAll('button[data-sort]');
    buttons.forEach(function(button){
        button.onclick = sorting;
    });
};