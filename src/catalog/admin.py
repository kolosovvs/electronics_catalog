from django.contrib import admin
from catalog.models import Country, Producer, Category, Product


class ProductAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_filter = ('producer', 'category',)


class ProductInline(admin.StackedInline):
    model = Product


class ProducerAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    inlines = [ProductInline,]


admin.site.register(Country)
admin.site.register(Producer, ProducerAdmin)
admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
