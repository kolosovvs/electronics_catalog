from django import forms


class SearchForm(forms.Form):
    q = forms.CharField(max_length=30)
    sort = forms.CharField(max_length=30, required=False)