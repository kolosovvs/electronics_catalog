from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin


urlpatterns = [
    url(r'^$', views.ProductView.as_view(), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/$', views.SearchView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.ProductDetail.as_view(), name='detail'),
    url(r'^([\w-]+)/$', views.CategoryProductList.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
