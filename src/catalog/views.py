from django.shortcuts import render
from django.views.generic import ListView, DetailView
from catalog.models import Country, Producer, Category, Product
from django.shortcuts import get_object_or_404
from django.views.generic import View
from catalog.forms import SearchForm
from django.db.models import Q


class ProductView(ListView):
    template_name = 'catalog/product_list.html'

    def get_queryset(self):
        qs = Product.objects.all()
        sort = self.request.GET.get('sort')
        if sort:
            qs = Product.sorting(qs, sort)
        return qs


class CategoryProductList(ListView):
    template_name = 'catalog/product_list.html'

    def get_queryset(self):
        self.category = get_object_or_404(Category, title=self.args[0])
        qs = Product.objects.filter(category=self.category)
        sort = self.request.GET.get('sort')
        if sort:
            qs = Product.sorting(qs, sort)
        return qs


class ProductDetail(DetailView):
    template_name = 'catalog/product_detail.html'
    model = Product


class SearchView(View):
    template_name = 'catalog/product_list.html'
    form_class = SearchForm

    def get(self, request):
        form = self.form_class(request.GET)
        if form.is_valid():
            q = form.cleaned_data['q']
            sort = form.cleaned_data['sort']
            qs = Product.objects.filter(Q(producer__title__icontains=q) |
                                        Q(title__icontains=q))
            if sort:
                qs = Product.sorting(qs, sort)
        else:
            qs = None
        return render(request, self.template_name, {'product_list': qs})