from django.db import models


class Country(models.Model):
    title = models.CharField(max_length=64)

    def __str__(self):
        return self.title


class Producer(models.Model):
    country = models.ForeignKey(Country, related_name='producers', blank=True, null=True)
    title = models.CharField(max_length=64)

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=64)

    def __str__(self):
        return self.title


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products')
    producer = models.ForeignKey(Producer, related_name='products', blank=True, null=True)
    title = models.CharField(max_length=64)
    price = models.FloatField()
    technical_info = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='itempics')

    @staticmethod
    def sorting(qs, sort):
        if sort == 'price':
            qs = qs.order_by('price')
        elif sort == 'price_rev':
            qs = qs.order_by('-price')
        elif sort == 'name':
            qs = qs.order_by('title')
        return qs

    def __str__(self):
        return self.title
